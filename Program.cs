﻿
namespace Algorithm.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    public class Program
    {
        /// <summary>
        /// PROBLEMA:
        /// 
        /// Implementar um algoritmo para o controle de posição de um drone em um plano cartesiano (X, Y).
        /// 
        /// O ponto inicial do drone é "(0, 0)" para cada execução do método Evaluate ao ser executado cada teste unitário.
        /// 
        /// A string de entrada pode conter os seguintes caracteres N, S, L, e O representando Norte, Sul, Leste e Oeste respectivamente.
        /// Estes catacteres podem estar presentes aleatóriamente na string de entrada.
        /// Uma string de entrada "NNNLLL" irá resultar em uma posição final "(3, 3)", assim como uma string "NLNLNL" irá resultar em "(3, 3)".
        /// 
        /// Caso o caracter X esteja presente, o mesmo irá cancelar a operação anterior. 
        /// Caso houver mais de um caracter X consecutivo, o mesmo cancelará mais de uma ação na quantidade em que o X estiver presente.
        /// Uma string de entrada "NNNXLLLXX" irá resultar em uma posição final "(1, 2)" pois a string poderia ser simplificada para "NNL".
        /// 
        /// Além disso, um número pode estar presente após o caracter da operação, representando o "passo" que a operação deve acumular.
        /// Este número deve estar compreendido entre 1 e 2147483647.
        /// Deve-se observar que a operação 'X' não suporta opção de "passo" e deve ser considerado inválido. Uma string de entrada "NNX2" deve ser considerada inválida.
        /// Uma string de entrada "N123LSX" irá resultar em uma posição final "(1, 123)" pois a string pode ser simplificada para "N123L"
        /// Uma string de entrada "NLS3X" irá resultar em uma posição final "(1, 1)" pois a string pode ser siplificada para "NL".
        /// 
        /// Caso a string de entrada seja inválida ou tenha algum outro problema, o resultado deve ser "(999, 999)".
        /// 
        /// OBSERVAÇÕES:
        /// Realizar uma implementação com padrões de código para ambiente de "produção". 
        /// Comentar o código explicando o que for relevânte para a solução do problema.
        /// Adicionar testes unitários para alcançar uma cobertura de testes relevânte.
        /// </summary>
        /// <param name="input">String no padrão "N1N2S3S4L5L6O7O8X"</param>
        /// <returns>String representando o ponto cartesiano após a execução dos comandos (X, Y)</returns>

        public static string Evaluate(string input)
        {
            string cartesiano = "(999, 999)";
            try
            {
                ///Validação para evitar entradas Nulas ou Strings Vazias
                if (!String.IsNullOrEmpty(input) && !String.IsNullOrWhiteSpace(input)) 
                {
                    #region Declaração de variaveis
                    List<string> lstMovimentos = new List<string>();
                    string movimento = string.Empty;
                    #endregion

                    #region Gera Lista de Movimentos
                    /// Tranforma String em Array seprados por Letras
                    string[] arrInput = Regex.Split(input, @"([A-Z])");
                    /// Remove Strings VAzias do Array
                    arrInput = arrInput.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                    /// For para converter o Array e deixar em um formato amigavel para tratamento
                    /// Input:  N123LSX
                    /// Output: [0] => N123
                    ///         [1] => L1
                    ///         [2] => S1
                    ///         [3] => X
                    for (int i = 0; i <= (arrInput.Count() - 1); i++)
                    {
                        movimento = string.Empty;
                        /// Indentifica se a variavel é uma letra (Padrão)
                        /// Se o Indice tiver um numero nessa entrada o Script é encerrado como inválido 
                        movimento = !arrInput[i].All(char.IsDigit) ? arrInput[i] : ""; 
                        if (!String.IsNullOrEmpty(movimento))
                        {
                            ///Verifica se existe um próximo indice no Array, e se esse indice é um número.
                            /// Se for verdade ele pega o Indice atual + o próximo indice(numero) EX: N123
                            /// Caso o retorno seja false, verifica se o indice é diferente de "X", insere o indice + 1 EX: L1
                            if (arrInput.ElementAtOrDefault(i + 1) != null && arrInput[i + 1].All(char.IsDigit))
                            {
                                int numPassos = Convert.ToInt32(arrInput[i + 1]);
                                if (numPassos >= 1 && numPassos <= 2147483647)
                                {
                                    movimento = movimento + arrInput[i + 1];
                                    i++;
                                }
                                else
                                    return cartesiano;
                            }
                            else
                                movimento = movimento != "X" ? movimento + "1" : movimento;

                            lstMovimentos.Add(movimento);
                        }
                        else
                        {
                            return cartesiano;
                        }
                    }
                    #endregion

                    lstMovimentos = RemoveMovimentos(lstMovimentos);
                    cartesiano = GeraPlanoCartesiano(lstMovimentos);
                }
                return cartesiano;
            }
            catch (Exception)
            {
                return "(999, 999)";
            }
        }

        /// <summary>
        /// Remove os movimentos "X" e seu antecessor.
        /// </summary>
        /// <param name="lstMovimentos"></param>
        /// <returns>Linsta de movimentos válidos</returns>
        private static List<string> RemoveMovimentos(List<string> lstMovimentos)
        {

            for (int i = 0; i <= (lstMovimentos.Count() - 1); i++)
            {
                if (lstMovimentos[i] == "X")
                {
                    lstMovimentos.RemoveRange((i - 1), 2);
                    i = i - 2;
                }
            }
            return lstMovimentos;
        }

        /// <summary>
        /// Gera Plano Cartesiano com os movimentos válidos  
        /// </summary>
        /// <param name="lstMovimentos"></param>
        /// <returns> Retorna Sting com fomato "(X, Y)</returns>
        private static string GeraPlanoCartesiano(List<string> lstMovimentos)
        {
            List<int> EixoX = new List<int>();
            List<int> EixoY = new List<int>();

            foreach (var movimento in lstMovimentos)
            {
                /// Separa a String em um array de 2 posições, LETRA e PASSOS
                /// INPUT: N123
                /// OUTPUT [0] => N
                ///        [1] => 123
                string[] arrMovimento = Regex.Split(movimento, @"([NSOL])").Where(x => !string.IsNullOrEmpty(x)).ToArray();

                ///Indetifica qual a LETRA (posição no Eixo cartesiano) o Indice pertence, 
                ///e adiciona a quantidade de passos ao mesmo.
                /// Se a Letra não for uma das posições validas (NSQL), retorna um valor INVÁLIDO
                switch (arrMovimento[0])
                {
                    case "N":
                        EixoY.Add(Convert.ToInt16(arrMovimento[1]));
                        break;
                    case "S":
                        EixoY.Add(-Convert.ToInt16(arrMovimento[1]));
                        break;
                    case "L":
                        EixoX.Add(Convert.ToInt16(arrMovimento[1]));
                        break;
                    case "O":
                        EixoX.Add(-Convert.ToInt16(arrMovimento[1]));
                        break;
                    default:
                        EixoY = new List<int>() { 999 };
                        EixoX = new List<int>() { 999 };
                        break;
                }

            }
            /// SOMA os valores adicionados aos Eixos X e Y
            int X = EixoX.Sum();
            int Y = EixoY.Sum();
            string cartesiano = "(" + Convert.ToString(X) + ", " + Convert.ToString(Y) + ")";

            return cartesiano;
        }

    }
}
